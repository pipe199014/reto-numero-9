#Author: fgomezv@choucairtesting.com


Feature: Realizar la consulta por Bancos en las Páginas Amarillas WEB y verificar para el listado 
         de los primeros 10 bancos que la página Web sea la esperada

  @ConsultarBanco
  Scenario: Realizar la consulta por Bancos en las Páginas Amarillas WEB 
            y verificar para el listado de los primeros 10 bancos que la página Web sea la esperada
       Given que quiero realizar la consulta de 10 "bancos"
       When se despliega la información de estos 
       Then Verifico que el link de la pagina web lleve al Banco Correspondiente

