package com.choucair.formacion.steps;

import org.fluentlenium.core.annotation.Page;

import com.choucair.formacion.pageobjects.PaginasAmarillasPage;

import net.thucydides.core.annotations.Step;

public class PaginasAmarillasSteps {
	
	@Page
	PaginasAmarillasPage paginasAmarillasPage;
	
	@Step
	public void ingresarPaginasAmarillas()
	{
		paginasAmarillasPage.open();
	}
	
	@Step
	public void realizoBusqueda(String palabra)
	{
		paginasAmarillasPage.realizoBusqueda(palabra);
	}
	
	@Step
	public void recorrerIngresar() throws Exception
	{
		paginasAmarillasPage.recorrerIngresar();
	}
	

}
