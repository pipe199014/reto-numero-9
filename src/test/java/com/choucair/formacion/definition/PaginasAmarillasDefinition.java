package com.choucair.formacion.definition;

import com.choucair.formacion.steps.PaginasAmarillasSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PaginasAmarillasDefinition {
	
	@Steps
	PaginasAmarillasSteps paginasAmarillasSteps;

	@Given("^que quiero realizar la consulta de 10 \"([^\"]*)\"$")
	public void que_quiero_realizar_la_consulta_de(String palabra) throws Throwable {
		paginasAmarillasSteps.ingresarPaginasAmarillas();
		paginasAmarillasSteps.realizoBusqueda(palabra);
	}

	@When("^se despliega la información de estos$")
	public void se_despliega_la_información_de_estos() throws Throwable {
		paginasAmarillasSteps.recorrerIngresar();
	}

	@Then("^Verifico que el link de la pagina web lleve al Banco Correspondiente$")
	public void verifico_que_el_link_de_la_pagina_web_lleve_al_Banco_Correspondiente() throws Throwable {

	}
	
}
