package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;

import java.sql.Driver;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.containsString;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.choucair.formacion.util.ExcelReader;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www.paginasamarillas.com.co/")
public class PaginasAmarillasPage extends PageObject {

	@FindBy(xpath = "//*[@id='searchForm']/div//*[@id='keyword']")
	private WebElementFacade txtBuscar;

	@FindBy(xpath = "//*[@id='searchForm']/div//*[@id='buscar']")
	private WebElementFacade btnBuscar;

	@FindBy(xpath = "//*[@class='row listingWrap margin-off']")
	private List<WebElementFacade> TblBancos;

	@FindBy(xpath = "//*[@class='title semibold']")
	private WebElementFacade tituloBanco;

	public void realizoBusqueda(String palabra) {
		txtBuscar.sendKeys(palabra);
		btnBuscar.click();
	}

	public void recorrerIngresar() throws Exception {
	 	
		int cont = 1;

		String Ruta = "src/test/resources/Datadriven/datosBanco.xlsx";
		String Hoja = "Hoja1";
		ExcelReader.setExcelFile(Ruta, Hoja);

		for (WebElement trElement : TblBancos) {
			java.util.List<WebElement> tblColumna = trElement.findElements(By.xpath("div//a[@class='webLink']"));

			for (WebElement tdElement2 : tblColumna) {
				if (cont <= 10) {
					String urlBanco = "";

					urlBanco = tdElement2.getText();

					ExcelReader.setCellData(cont, 1, urlBanco);

					tdElement2.click();
					Thread.sleep(15000);
					String parentWindow = this.getDriver().getWindowHandle();
					Set<String> allWindows = getDriver().getWindowHandles();
					Integer intW = 0;
					int contador = 0;
					for (String curWindow : allWindows) {

						if (contador != 0) {
							if (intW == cont) {
								getDriver().switchTo().window(curWindow);
								break;
							}
						}
						intW++;
						contador++;
					}

					String tituloEspera = getDriver().getCurrentUrl();
					tituloEspera = tituloEspera.substring(0, tituloEspera.length() - 1).replace("https", "http").replace("o/e", "o").replace("/wps/portal/persona", "");

					getDriver().switchTo().window(parentWindow);
				
					assertThat(urlBanco, containsString(tituloEspera));

				}
				cont++;
			}

		}
		ExcelReader.SaveData(Ruta);
		ExcelReader.CerrarBook();
	}

}
